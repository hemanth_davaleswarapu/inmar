# Inmar Application

## Getting Started - Development

---

Before getting started make sure you have successfully completed docker installizations in the system(Laptop or Desktop)
### Clone the repo

First step is to clone the repo

```bash
git clone https://hemanth_davaleswarapu@bitbucket.org/hemanth_davaleswarapu/inmar.git
```

This repo consists of module,representing a part of the Inmar application.

### Afer Installing Docker and pulling the repo

To run or to start the services use start script if your are facing any permission issue:
```bash
chmod 777 start.sh
```
```bash
./start.sh or docker-compose up --build
```
To stop the services use stop script if your are facing any permission issue:
```bash
chmod 777 stop.sh 
```
```bash
./stop.sh or docker-compose down
```