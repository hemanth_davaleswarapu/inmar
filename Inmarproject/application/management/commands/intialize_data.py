import csv

from copy import deepcopy

from django.core.management.base import BaseCommand
from django.db import IntegrityError

from application.models import Sku, LOCATION, Department, Category, Subcategory


class Command(BaseCommand):
    help = "Initializes the system on startup or restart with Data"

    def handle(self, *args, **options):
        self.create_default_system_account()


    def create_default_system_account(self):
        """
        Initializes the system with some sku data
        """
        sku = Sku.objects.all()
        if not sku:
            csvfile = open('sku.csv', 'r')
            fieldnames = ("sku", "name", "location", "department", "category", "subcategory")
            reader = csv.DictReader(csvfile, fieldnames)
            query_list = deepcopy(list(reader))[1:-1]
            for item in query_list:
                try:
                    item['location'] = LOCATION.objects.create(**{'location': item['location']})
                except IntegrityError:
                    item['location'] = LOCATION.objects.get(location=item['location'])
                try:
                    item['department'] = Department.objects.create(**{'department': item['department']})
                except IntegrityError:
                    item['department'] = Department.objects.get(department=item['department'])
                try:
                    item['category'] = Category.objects.create(**{'category': item['category']})
                except IntegrityError:
                    item['category'] = Category.objects.get(category=item['category'])
                try:
                    item['subcategory'] = Subcategory.objects.create(**{'subcategory': item['subcategory']})
                except IntegrityError:
                    item['subcategory'] = Subcategory.objects.get(subcategory=item['subcategory'])
            objs = [
                Sku(**data)
                for data in query_list
            ]
            Sku.objects.bulk_create(objs=objs)
