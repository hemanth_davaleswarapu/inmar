from django.urls import path

from application.views import LocationPreferences, SkuInfo, LoactionInfo, DepartmentInfo,CategoryInfo,SubcategoryInfo

urlpatterns = [
     path("api/v1/location/", LoactionInfo.as_view()),
     path("api/v1/department/", DepartmentInfo.as_view()),
     path("api/v1/category/", CategoryInfo.as_view()),
     path("api/v1/subcategory/", SubcategoryInfo.as_view()),
     path("api/v1/location/<location>/department/", LocationPreferences.as_view()),
     path("api/v1/location/<location>/department/<department>/category/", LocationPreferences.as_view()),
     path("api/v1/location/<location>/department/<department>/category/<category>/subcategory/", LocationPreferences.as_view()),
     path("api/v1/location/<location>/department/<department>/category/<category>/subcategory/<subcategory>/", LocationPreferences.as_view()),
     path("api/v1/get_sku/", SkuInfo.as_view())
]
