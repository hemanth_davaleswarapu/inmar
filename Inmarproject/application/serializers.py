from rest_framework import serializers

from application.models import Sku, LOCATION, Department, Category, Subcategory

class SkuSerializer(serializers.ModelSerializer):
    location = serializers.CharField(source='location.location')
    category = serializers.CharField(source='category.category')
    department = serializers.CharField(source='department.department')
    subcategory = serializers.CharField(source='subcategory.subcategory')

    class Meta:
        model = Sku
        fields = ('location', 'category', 'department', 'subcategory')


class LocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = LOCATION
        fields = "__all__"


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = "__all__"


class SubcategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Subcategory
        fields = "__all__"
