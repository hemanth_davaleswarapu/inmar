from django.db import models


class LOCATION(models.Model):
    location = models.CharField(max_length=255, unique=True)


class Department(models.Model):
    department = models.CharField(max_length=255, unique=True)


class Category(models.Model):
    category = models.CharField(max_length=255, unique=True)


class Subcategory(models.Model):
    subcategory = models.CharField(max_length=255, unique=True)


class Sku(models.Model):
    sku = models.IntegerField()
    name = models.CharField(max_length=255)
    location = models.ForeignKey(LOCATION,
                                 blank=True,
                                 null=True,
                                 on_delete=models.CASCADE)
    category = models.ForeignKey(Category,
                                 blank=True,
                                 null=True,
                                 on_delete=models.CASCADE)
    department = models.ForeignKey(Department,
                                   blank=True,
                                   null=True,
                                   on_delete=models.CASCADE)
    subcategory = models.ForeignKey(Subcategory,
                                    blank=True,
                                    null=True,
                                    on_delete=models.CASCADE)

