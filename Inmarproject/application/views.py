from django.db.models import Q

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from application.models import Sku, LOCATION, Department, Category, Subcategory
from application.serializers import SkuSerializer, LocationSerializer, CategorySerializer, DepartmentSerializer, \
    SubcategorySerializer


class LocationPreferences(APIView):

    def build_query(self, params_dict):
        "Generating Dynamic ORM"

        q_objects = Q()
        for key, value in params_dict.items():
            if value != None:
                q_objects.add((Q(**{key: value})), q_objects.connector)
        return q_objects

    def get(self, request, location=None, department=None, category=None, subcategory=None):
        """Retrieve sku data bases on path paramas"""

        if location or department or category:
            params_dict = {"location": location, "department": department, "category": category,
                           "subcategory": subcategory}
            q_objects = self.build_query(params_dict)
            products = Sku.objects.filter(q_objects)
        else:
            products = Sku.objects.all()
        serializer = SkuSerializer(products, many=True)
        return Response(serializer.data)

    def post(self, request):
        """Post the data in sku table"""
        data = request.data
        serializer = SkuSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, location=None, department=None, category=None, subcategory=None):
        """Delete sku data bases on path paramas"""

        params_dict = {"location": location, "department": department, "category": category,
                       "subcategory": subcategory}
        q_objects = self.build_query(params_dict)
        products = Sku.objects.filter(q_objects)
        if products:
            products.delete()
            return Response({"Message": "Successfully deleted"}, status=status.HTTP_200_OK)
        return Response({"Message": "No records found!"}, status=status.HTTP_400_BAD_REQUEST)


class SkuInfo(APIView):

    def get(self, request):
        """Retrieve sku locations bases on query paramas(Meta Data in url)"""

        params_dict = {"location": request.GET.get("location", None),
                       "department": request.GET.get("department", None),
                       "category": request.GET.get("category", None),
                       "subcategory": request.GET.get("subcategory", None)}
        q_objects = Q()
        for key, value in params_dict.items():
            if value != None:
                q_objects.add((Q(**{key: value})), q_objects.connector)
        products = Sku.objects.filter(q_objects)
        serializer = SkuSerializer(products, many=True)
        return Response(serializer.data)


class LoactionInfo(APIView):

    def get(self, request, *args, **kwargs):
        """Retrieve all locations"""
        locations = LOCATION.objects.all()
        serializer = LocationSerializer(locations, many=True)
        return Response(serializer.data)


class DepartmentInfo(APIView):

    def get(self, request, *args, **kwargs):
        """Retrieve all departments"""
        departments = Department.objects.all()
        serializer = DepartmentSerializer(departments, many=True)
        return Response(serializer.data)


class CategoryInfo(APIView):

    def get(self, request, *args, **kwargs):
        """Retrieve all departments"""
        category = Category.objects.all()
        serializer = CategorySerializer(category, many=True)
        return Response(serializer.data)


class SubcategoryInfo(APIView):

    def get(self, request, *args, **kwargs):
        """Retrieve all subcategory"""
        subcategory = Subcategory.objects.all()
        serializer = SubcategorySerializer(subcategory, many=True)
        return Response(serializer.data)
